module Tarea12 where
{- TEMA: ARBOLES BINARIOS. FUNCIONES NO RECURSIVAS
   CANTIDAD DE UNDEFINEDs: 3
   Román García -}

{- NOTAS:
   * La definicion que vamos a usar de arbol binario es un poquito diferente
     de la vista en clase
-}

-- Tipo del arbol que vamos a usar:
data Tree a = Empty | Node a (Tree a) (Tree a) deriving Show

-- Construyo un arbol binario con numeros enteros
arbol = Node 1 (Node 3 Empty Empty)
               (Node 8 (Node 2 Empty Empty)
                       (Node 5 Empty Empty)) :: Tree Int
--                  1
--             3        8
--                    2   5


-- Determina si el arbol esta vacio (simil isEmpty de listas)
--TEST: estaVacio arbol -> False
estaVacio :: Tree a -> Bool
estaVacio Empty = True
estaVacio   _   = False


-- Dada un arbol, devuelve el dato que contiene la raiz (simil head de listas)
-- Parcial si el arbol esta vacio
--TEST: item arbol -> 1
item :: Tree a -> a
item (Node a t1 t2) = a


-- Devuelve el arbol izquierdo de un arbol no vacio. (simil tail de listas)
-- Parcial si el arbol esta vacio
--TEST: hijoIzq arbol -> Node 3 Empty Empty
hijoIzq :: Tree a -> Tree a
hijoIzq (Node a t1 t2) = t1


-- Devuelve el arbol derecho de un arbol no vacio. (simil tail de listas)
-- Parcial si el arbol esta vacio
--TEST: hijoDer arbol -> Node 8 (Node 2 Empty Empty) (Node 5 Empty Empty)
hijoDer :: Tree a -> Tree a
hijoDer = undefined


-- Determima si un arbol no vacio es una hoja, o sea que no tiene subarboles
-- Parcial si el arbol esta vacio
-- VERSION usando pattern matching
--TEST: esHoja arbol -> False
--TEST: esHoja (hijoIzq arbol) -> True
esHoja :: Tree a -> Bool
esHoja (Node a Empty Empty) = True
esHoja         _            = False

-- VERSION sin usar pattern matching
esHoja' :: Tree a -> Bool
esHoja' t = estaVacio (hijoIzq t) && estaVacio (hijoDer t)


-- Dada un arbol, devuelve el valor ubicado en la rama derecha
-- Precondicion: El arbol tiene una rama derecha
{-NOTA: ACA HICE UN DOBLE PATERN MATCHING-}
--TEST: itemArbolDer arbol -> 8
itemArbolDer :: Tree a -> a
itemArbolDer (Node _ t1 (Node a _ _)) = a

-- Hacer la misma funcion pero sin usar pattern matching
itemArbolDer' :: Tree a -> a
itemArbolDer' = undefined
