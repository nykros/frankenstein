module Tarea14 where
import Tarea12 (Tree(..),esHoja,arbol)
import Tarea01 (esPar)
{- TEMA: ARBOLES BINARIOS. MAS FUNCIONES RECURSIVAS SIMPLES
   CANTIDAD DE UNDEFINEDs: 3
   Román García -}

-- Dado un arbol de enteros, determinar la cantidad de numeros pares que tiene
--TEST: cantPares arbol -> 2
cantPares :: Tree Int -> Int
cantPares     Empty      = 0
cantPares (Node a t1 t2) = if (esPar a)
                            then 1 + cantPares t1 + cantPares t2
                            else     cantPares t1 + cantPares t2


-- Devuelve el item que se encuentra mas a la derecha del arbol
-- Parcial si el arbol es vacio
--TEST: itemMasDerecho arbol -> 5
itemMasDerecho :: Tree a -> a
itemMasDerecho (Node x t1 Empty) = x
itemMasDerecho (Node x t1 t2)    = itemMasDerecho t2


-- Dado un arbol de enteros, devolver True si todos los elementos son pares
--TEST: todosParesT arbol -> False
todosParesT :: Tree Int -> Bool
todosParesT = undefined


-- Devuelve el item que se encuentra mas a la izquierda del arbol
-- Parcial si el arbol es vacio
--TEST: itemMasIzquierdo arbol -> 3
itemMasIzquierdo :: Tree a -> a
itemMasIzquierdo = undefined


-- Devuelve la cantidad de hojas del arbol
--TEST: cantHojas arbol -> 3
cantHojas :: Tree a -> Int
cantHojas = undefined
