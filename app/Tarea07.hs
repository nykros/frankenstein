module Tarea07 where
import Tarea03(menoresQue,mayoresOIgualesQue)
{- TEMA: RECURSION SOBRE LISTAS (MAS AVANZADA)
   CANTIDAD DE UNDEFINEDs: 6
   Román García -}

{- NOTAS:
   * Estos ejercicios son de recursion pura, que no siguen un patron en particular.
   * NO SE COMPLIQUEN SI NO LES SALEN, SIGAN CON LA PROXIMA TAREA
-}

-- Dada una lista, devuelve los primeros n elementos de la lista. {-AKA: take -}
-- Parcial si n<0
--TEST: tomar 3 [4,3,5,3,2] -> [4,3,5]
tomar :: Int -> [a] -> [a]
tomar 0   _    = []
tomar _   []   = []
tomar n (x:xs) = x : tomar (n-1) xs


-- Dada un lista, la devuelve ordenada. Implementa un quicksort
--TEST: ordenar [4,2,-8,4,9,3] -> [-8,2,3,4,4,9]
ordenar :: Ord a => [a] -> [a]
ordenar   [] = []
ordenar (x:xs) = ordenar (menoresQue x xs) ++ [x] ++ ordenar (mayoresOIgualesQue x xs)


-- Dada una lista, devueve la lista sin los primeros n elementos. {-AKA: drop -}
--TEST: dejar 3 [4,3,5,3,2] -> [3,2]
dejar :: Int -> [a] -> [a]
dejar = undefined


-- Devuelve true si la segunda lista comienza con los elementos de la primera
--TEST: comienzaCon "Alla" "Alla vamos" -> True
--TEST: comienzaCon ""     "Alla vamos" -> True
--TEST: comienzaCon "Ala " "Alla vamos" -> False
comienzaCon :: Eq a => [a] -> [a] -> Bool
comienzaCon = undefined


-- Dada una lista, devuelve la lista al reves. {-AKA: reverse -}
--TEST: reversa [4,3,9,19] -> [19,9,3,4]
reversa :: [a] -> [a]
reversa = undefined


-- Dadas dos listas, devueve la lista que es la concatenacion de ambas {-AKA: ++ -}
-- TEST unir [1,2,3] [4,5] -> [1,2,3,4,5]
unir :: [a] -> [a] -> [a]
unir = undefined


-- Dada una lista de listas, devuelve la lista con los datos de las sublistas {-AKA: concat -}
--TEST: concatenar ["to","be","or"] -> "tobeor"
concatenar :: [[a]] -> [a]
concatenar = undefined


-- Dado un numero n y un elemento, devuelve una lista con n elementos. {-AKA: replicate -}
--TEST: replicar 4 False -> [False,False,False,False]
replicar ::  Int -> a -> [a]
replicar = undefined


-- Dada una lista, devueve la lista sin el ultimo elemento. {-AKA: init -}
-- Parcial cuando esta vacia
--TEST: sinUltimo [2,4,5] -> [2,4]
sinUltimo :: [a] -> [a]
sinUltimo   [x]  = []
sinUltimo (x:xs) = x: sinUltimo xs
