module Tarea23 where
import TAD_Cola
import Tarea22(mostrarCola)
import Tarea09(Persona(..),juan,pedro,jose)
{- TEMA: TIPOS DE DATOS ABSTRACTOS: COLAS + PERSONAS
   CANTIDAD DE UNDEFINEDs: 2
   Román García -}

{- NOTAS:
   * Vamos a darle una vuelta de tuercas. Un cliente es una persona de la cual
     sabemos cuantos minutos va a tardar su tramite.
   * Una cola es una Queue de clientes (OJO! no confundir)
-}

data Cliente = MkCliente Persona Minutos deriving Show
type Cola = Queue Cliente
type Minutos = Int

tiempo :: Cliente -> Int
tiempo (MkCliente p m) = m

persona :: Cliente -> Persona
persona (MkCliente p m) = p

-- Creo un valores de tipo Cliente y de tipo Cola
cJuan  = MkCliente juan 5
cPedro = MkCliente pedro 8
cJose  = MkCliente jose 3
cola = enqueueQ cJose (enqueueQ cPedro (enqueueQ cJuan emptyQ))

-- Cuanto tiempo nos llevará atender la cola?
--TEST: tiempoAtencion cola -> 16
tiempoAtencion :: Cola -> Minutos
tiempoAtencion q | isEmptyQ q = 0
                 | otherwise  = tiempo(firstQ q) + tiempoAtencion (dequeueQ q)


-- Devuelve True si un cliente de cierto nombre está en la Cola
--TEST: estaClienteLlamado "Juan" cola -> True
--TEST: estaClienteLlamado "Roman" cola -> False
estaClienteLlamado :: String -> Cola -> Bool
estaClienteLlamado = undefined


-- Cual es la persona que menos tiempo lleva su tramite.  Suponer que al menos
-- hay un cliente en la cola.
--TEST: elDeTramiteMasCorto cola -> Persona "Jose" 10
elDeTramiteMasCorto :: Cola -> Persona
elDeTramiteMasCorto = undefined


-- Cuantas clientes puedo atender en n minutos?
-- Suponer que el tiempo que tarda en atenderse la cola completa es mayor que el tiempo consultado
-- EJERCICIO OPCIONAL
--TEST: cuantosPuedoAtenderEn 4 cola -> 0
--TEST: cuantosPuedoAtenderEn 5 cola -> 1
--TEST: cuantosPuedoAtenderEn 10 cola -> 1
--TEST: cuantosPuedoAtenderEn 15 cola -> 2
cuantosPuedoAtenderEn :: Minutos -> Cola -> Int
cuantosPuedoAtenderEn = undefined
