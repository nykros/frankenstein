module Tarea25 where
import TAD_Map
import Data.Maybe
{- TEMA: TIPOS DE DATOS ABSTRACTOS: MAP
   CANTIDAD DE UNDEFINEDs: 4
   Román García -}

{- NOTAS:
   * Un Map se lo puede pensar como un conjunto de asociaciones (clave,valor)
     que se pueden buscar por clave.
     Un buen ejemplo podria ser la guia de telefono. Nos interesa poner en la
     guia el nombre de las personas y el numero de telefono que tienen. Lo que
     nos interesa saber es el numero de telefono de la persona.

     EN GENERAL, LA PRINCIPAL MOTIVACION PARA USAR MAP ES DE EFICIENCIA.
       -Si la guia se la implementa como una lista sin orden y tiene 100.000
       numeros de telefono y se la consulta mil veces, en el peor de los casos
       hay que hacer cien millones de comparaciones.
       -Si la guia se implementa como un map basado en AVL, solo hay que hacer
         16.609 comparaciones (mas o menos)

     La interface de map es la que aparece en la guia 5 nueva:
        emptyM  :: Ord k => Map k v                       O(1)
        assocM  :: Ord k => Map k v -> k -> v -> Map k v  O(log n)
        lookupM :: Ord k => Map k v -> k -> Maybe v       O(log n)
        deleteM :: Ord k => Map k v -> k -> Map k v       O(log n)
        domM    :: Ord k => Map k v -> [k]                O(n)

        LOS ORDENES CORRESPONDEN A LA IMPLEMENTACION BASADA EN AVL

    *El tipo Maybe se define en el modulo Data.Maybe como
         data Maybe a = Nothing | Just a
     y se usa para denotar la posibilidad que algo salga mal o no exista. Por
     ejemplo la funcion lookupM devuelve Nothing si el elemento no esta en el
     map o devuelve un Just con el resultado encontrado adentro.
     Las funciones mas utiles sobre Maybe son:

       -- Devuelve True si es un nothing
       isNothing :: Maybe a -> Bool
       isNothing Nothing = True
       isNothing    _    = False

       -- Dada un Maybe que sea un Just de algo, devuelve el algo. Parcial.
       fromJust :: Maybe a -> a
       fromJust (Just a) = a
-}

-- Como usuarios de Map, implementar la guia telefonica:

type Guia = Map Nombre Int
type Nombre = String
type Numero = String

guia = assocM (assocM emptyM "Juan" 452014) "Pedro" 454951 :: Guia


-- Dado un Map, lo convierte en un string que puede ser mostrado
--TEST: mostrarMap guia -> {"Juan"->452014, "Pedro"->454951}
mostrarMap :: (Ord k, Show k, Show v) => Map k v -> String
mostrarMap m = "{" ++ mostrarListaDePares (mapToPares m) ++ "}"

-- Muestro la lista de pares separadas con coma
mostrarListaDePares []       = ""
mostrarListaDePares [(k,v)]      = show k ++ "->" ++ show v
mostrarListaDePares ((k,v):y:ys) = show k ++ "->" ++ show v ++ ", " ++ mostrarListaDePares (y:ys)

-- Dado un mapa, devuelve el contenido del mapa como una lista de pares
mapToPares :: (Ord k, Show k, Show v) => Map k v -> [(k,v)]
mapToPares m = go (domM m)
  where go   []   = []
        go (x:xs) = (x,fromJust(lookupM m x)) : go xs


-- Dada una lista de pares (clave,valor) los pone en un Map clave valor
--TEST: mostrarMap (ponerEnMap [(2,4),(3,9)]) -> "{2->4, 3->9}"
ponerEnMap :: Ord k => [(k,v)] -> Map k v
ponerEnMap  = undefined


-- Determina si una persona tiene o no telefono
--TEST: tieneTelefono "Juan" guia -> True
--TEST: tieneTelefono "Luz" guia -> False
tieneTelefono :: Nombre -> Guia -> Bool
tieneTelefono = undefined


-- Dada una lista de Maybe a, devuelve una lista de a
--TEST: limpiarNothings [Nothing, Maybe 17, Maybe 43] -> [17,43]
limpiarNothings :: [Maybe a] -> [a]
limpiarNothings = undefined


-- Dada una lista de nombres y una guia, devolver los telefonos de las personas
-- de la lista (si tienen telefono)
--TEST: seleccionarTelefonos ["Jose", "Pedro", "Paco"] guia -> [454951]
seleccionarTelefonos :: [String] -> Guia -> [Numero]
seleccionarTelefonos = undefined
