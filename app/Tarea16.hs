module Tarea16 where
import Tarea12 (Tree(..))
import Tarea13 (listarInOrder,alturaT)

{- TEMA: ARBOLES BINARIOS DE ALGO
   CANTIDAD DE UNDEFINEDs: 4
   Román García -}

{- NOTAS:
   * Notar que el club ES un arbol binario (no que contiene un arbol binario).
     No es un DATA, es un TYPE. No son necesarias funciones auxiliares para
    "tapar" y "destapar"
-}

-- Se quiere representar la informacion de los socios de un club de la
-- siguiente forma:

type Club = Tree Socio
data Socio = MkSocio Nombre Edad [Deporte] EstaAlDia deriving Show

type Nombre = String
type Edad = Int
data Deporte = Football | Tenis | Basquet deriving (Eq,Show)
type EstaAlDia = Bool

-- Creo algunos valores:

facu = MkSocio "Facundo"  18 [Football,Tenis] True
sole = MkSocio "Soledad"  25 [] False
fede = MkSocio "Federico" 44 [] True
club = Node facu (Node sole Empty Empty)
                 (Node fede Empty Empty)

{-- Definir funciones de acceso --}

nombre :: Socio -> Nombre
nombre (MkSocio nom edad deps alDia) = nom

edad :: Socio -> Edad
edad (MkSocio nom edad deps alDia) = edad

deportes :: Socio -> [Deporte]
deportes (MkSocio nom edad deps alDia) = deps

estaAlDia :: Socio -> EstaAlDia
estaAlDia (MkSocio nom edad deps alDia) = alDia


{-- Definir las siguientes funciones --}

-- Dado un club, devuele una lista de los socios que no esten al dia
--TEST: listarMorosos club -> [MkSocio "Soledad" 25 [] False]
listarMorosos :: Club -> [Socio]
listarMorosos cl = filtrarMorosos (listarInOrder cl)

--auxiliar: Se como hacerlo en lista mas facilmente.
filtrarMorosos :: [Socio] -> [Socio]
filtrarMorosos      []        = []
filtrarMorosos (socio:socios) = if not (estaAlDia socio)
                                   then socio : filtrarMorosos socios
                                   else         filtrarMorosos socios


-- Determinar la altura de arbol. Ya LO HICE!
--TEST: alturaArbolClub club -> 2
alturaArbolClub :: Club -> Int
alturaArbolClub club = alturaT club


-- Dado un club determina la cantidad de integrantes que tiene
--TEST: cantIntegrantes club -> 3
cantIntegrantes :: Club -> Int
cantIntegrantes = undefined


-- Suponiendo que la proxima cuota es de $100.00 Dada un club, determinar cuanto
-- se va a recaudar en cuotas si los que estan al dia pagan
--TEST: recaudacionProximaCuota club -> 200.0
recaudacionProximaCuota :: Club -> Double
recaudacionProximaCuota = undefined


-- Dado un Club, devuelve una lista de socios que practican un deporte dado
--TEST: practican Football club -> [ MkSocio "Facundo"  18 [Football,Tenis] True]
practican :: Deporte -> Club -> [Socio]
practican = undefined


-- Dado un club devolver un arbol con la misma estructura que el club
-- con las edades de los socios. ESTO ES UN MAP SOBRE ARBOLES
--TEST: clubToTreeEdad club -> Node 18 (Node 25 Empty Empty) (Node 44 Empty Empty)
clubToTreeEdad :: Club -> Tree Edad
clubToTreeEdad Empty = Empty
clubToTreeEdad (Node socio t1 t2) = Node (edad socio) (clubToTreeEdad t1) (clubToTreeEdad t2)


-- Dada un club, devolver un arbol con los deportes que practican cada socio
--TEST: clubToTreeActividades club -> Node [Football,Tenis] (Node [] Empty Empty) (Node [] Empty Empty)
clubToTreeActividades :: Club -> Tree [Deporte]
clubToTreeActividades = undefined
