module TAD_Conjunto (Conjunto,vacioC,agregarC,perteneceC,cantidadC,borrarC,unionC,listarC) where
{- Implementacion de Conjuntos con listas según interface de la Practica4
   INVARIANTE DE REPRESENTACION: en todo conjunto (MkSet xs n),
      - xs no tiene elementos repetidos
      - longitudL xs == n

   CANTIDAD DE UNDEFINEDs: 6
   Román García -}

data Conjunto a = MkSet [a] Int

-- Crea un conjunto vacio
vacioC :: Conjunto a
vacioC = MkSet [] 0


-- Dados un elemento y un conjunto, agrega el elemento al conjunto
agregarC :: Eq a => a -> Conjunto a -> Conjunto a
agregarC = undefined


-- Dado un elemento y un conjunto, indica si el elemento pertenece al conjunto
perteneceC :: Eq a => a -> Conjunto a -> Bool
perteneceC = undefined


-- Devuelve la cantidad de elementos distintos de un conjunto
cantidadC :: Eq a => Conjunto a -> Int
cantidadC = undefined


-- Saca del conjunto el elemento dado
borrarC :: Eq a => a -> Conjunto a -> Conjunto a
borrarC = undefined


-- Dados dos conjuntos devuelve un conjunto con todos los elementos de ambos conjunto
unionC :: Eq a => Conjunto a -> Conjunto a -> Conjunto a
unionC = undefined


--Dado un conjunto devuelve una lista con todos los elementos distintos del Conjunto
listarC :: Eq a => Conjunto a -> [a]
listarC = undefined
