module Tarea13 where
import Tarea12 (Tree(..),arbol)
{- TEMA: ARBOLES BINARIOS. FUNCIONES RECURSIVAS SIMPLES
   CANTIDAD DE UNDEFINEDs: 6
   Román García -}

{- NOTAS:
   * Se puede pensar que un arbol binario como una lista con dos colas. Si para
     sumar todos los items de un lista se debia sumar el primer item y el resto,
     ahora se debe sumar la raiz, y los dos restos (el hijo izquierdo y el
     hijo derecho). De la misma forma que el resto de una lista era otra lista,
     los restos de los arboles son arboles.
-}

-- Dado un arbol binario de enteros devuelve la suma de sus elementos
--TEST: sumarT arbol -> 19
sumarT :: Tree Int -> Int
sumarT      Empty     = 0
sumarT (Node n t1 t2) = n + sumarT t1 + sumarT t2


-- Dada un arbol binario devuelve la cantidad de elementos que tiene
--TEST: tamanioT arbol -> 5
tamanioT :: Tree a -> Int
tamanioT = undefined


--Dado un elemento y un arbol binario devuelve True si dicho elemento esta en el arbol
--TEST: perteneceT 4 arbol -> False
--TEST: perteneceT 5 arbol -> True
perteneceT :: Eq a => a -> Tree a -> Bool
perteneceT = undefined


-- Devuelve la cantidad de veces que un elemento aparece en el arbol
--TEST: aparicionesT 4 arbol -> 0
--TEST: aparicionesT 5 arbol -> 1
aparicionesT :: Eq a => a -> Tree a -> Int
aparicionesT = undefined


-- Devuelve la altura de un arbol. La altura de un arbol vacio es cero. La altura
-- de un arbol no vacio es la 1 mas la mayor altura de sus hijos
--TEST: alturaT arbol -> 3
alturaT :: Tree a -> Int
alturaT = undefined


--------------  Las siguientes funciones convierten un arbol en una lista. -------------

-- Primero se procesa el hijo izquierdo, luego el item de la raiz y luego el hijo derecho
--TEST: listarInOrder arbol -> [3,1,2,8,5]
listarInOrder :: Tree a -> [a]
listarInOrder     Empty      = []
listarInOrder (Node a t1 t2) = listarInOrder t1 ++ [a] ++ listarInOrder t2


-- Primero se procesa el item de la raiz, luego el hijo izquierdo y luego el hijo derecho
--TEST: listarPreOrder arbol -> [1,3,8,2,5]
listarPreOrder :: Tree a -> [a]
listarPreOrder = undefined


-- Primero se procesa el hijo izquierdo, luego el hijo derecho y luego item de la raiz
--TEST: listarPosOrder arbol -> [3,2,5,8,1]
listarPosOrder :: Tree a -> [a]
listarPosOrder = undefined
