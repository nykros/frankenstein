module Tarea15 where
import Tarea12 (Tree(..),arbol)
import Tarea13 (listarInOrder)
import Tarea05 (masChico,masGrande)
{- TEMA: ARBOLES BINARIOS. COMPOSICION DE FUNCIONES
   CANTIDAD DE UNDEFINEDs: 5
   Román García -}

{- NOTAS:
   * IMPLEMENTAR ESTAS FUNCIONES SIN RECURSION EXPLICITA
   * Se podria decir que estas funciones no son recursivas, pero eso no es
     completamente cierto, porque la unica forma de recorrer un arbol es
     recursivamente, pero la recursion la hace alguna otra funcion.
   * No repetir codigo, fijense que cosas ya tienen definidas
   * Modificar los import segun sea necesario para no repetir codigo
-}

-- Dada un arbol, determinar el item mas pequeño
--TEST: masChicoT arbol -> 1
masChicoT :: Ord a => Tree a -> a
masChicoT t = masChico (listarInOrder t)


-- Dada un arbol, determinar el mayor elemento del arbol
--TEST: masChicoT arbol -> 8
masGrandeT :: Ord a => Tree a -> a
masGrandeT = undefined


-- Calcular el promedio de un arbol enteros
--TEST: promedioT arbol -> 3
promedioT :: Tree Int -> Int
promedioT = undefined


--Dado un elemento y un arbol binario devuelve True si dicho elemento esta en el arbol
--TEST: perteneceT' 4 arbol -> False
--TEST: perteneceT' 5 arbol -> True
perteneceT' :: Eq a => a -> Tree a -> Bool
perteneceT' = undefined


-- Dados dos elementos, devuelve el que se repite mas veces en el arbol
--TEST: elQueMasSeRepite arbol -> como todos estan una sola vez, depende de
--                                como se implemente, el valor puede es cualquiera
--                                de los items del arbol, en este caso particular
elQueMasSeRepite :: Eq a => a -> a -> Tree a -> a
elQueMasSeRepite = undefined


-- Dado un arbol de booleanos, indicar si todos los items del arbol son True
-- todoVerdadT (Node True Empty Empty)  -> True
-- todoVerdadT (Empty)                  -> True
-- todoVerdadT (Node False Empty Empty) -> False
todoVerdadT :: Tree Bool -> Bool
todoVerdadT = undefined
