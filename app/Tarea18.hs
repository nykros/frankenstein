module Tarea18 where

{- TEMA: ARBOLES BINARIOS MAQUILLADOS II: recordando el litoral
   CANTIDAD DE UNDEFINEDs: 2
   Román García -}

{- NOTAS:
   * Esta tarea esta basada en el segundo parcial que se tomo
   * Se puede ver que es un arbol binario (Rio aparece dos veces a la derecha de
     su definicion) con un solo caso base (que se llama Delta)
-}

-- Se tienen los siguientes tipos de datos para representar una red fluvial

data Rio = Delta Caudal [Barco] | Bifurcacion Caudal [Barco] Rio Rio deriving (Show)
data Barco = Pesquero | Carguero | Petrolero deriving (Show,Eq)
type Caudal = Int


-- Defino un valor del tipo Rio:

rio = Bifurcacion 100 [Pesquero]
          (Bifurcacion 100 [] (Delta 50 [Pesquero])
                              (Delta 25 []))
          (Bifurcacion 125 [] (Delta 10 [Pesquero,Carguero])
                              (Delta 5 [Petrolero]))


-- Que dada una clase de barco y un rio, retorna la lista con todos los barcos
-- de ese tipo en la red fluvial
--TEST: todosLosBarcos Pesquero rio -> [Pesquero,Pesquero,Pesquero]
--TEST: todosLosBarcos Carguero rio -> [Carguero]
todosLosBarcos :: Barco -> Rio -> [Barco]
todosLosBarcos b       (Delta c bs)       = filtrarBarcos b bs
todosLosBarcos b (Bifurcacion c bs r1 r2) = filtrarBarcos b bs ++ todosLosBarcos b r1 ++ todosLosBarcos b r2

--auxiliar: es un simple filter de listas
filtrarBarcos :: Barco -> [Barco] -> [Barco]
filtrarBarcos b   []   = []
filtrarBarcos b (x:xs) = if (b == x)
                           then x : filtrarBarcos b xs
                           else     filtrarBarcos b xs


--Que dado un rio retorna el caudal total de toda la red fluvial que de el se desprende
--TEST: caudalTotal rio -> 415
caudalTotal :: Rio -> Caudal
caudalTotal = undefined


-- Que dada una clase de barco y un rio, quita todos los barcos de esa clase de la red fluvial
--TEST: hundirBarcos Pesquero rio ->
--       Bifurcacion 100 []
--          (Bifurcacion 100 [] (Delta 50 [])
--                              (Delta 25 []))
--          (Bifurcacion 125 [] (Delta 10 [Carguero])
--                              (Delta 5 [Petrolero]))
hundirBarcos ::Barco -> Rio -> Rio
hundirBarcos = undefined

-- Aumenta el caudal de los deltas de un rio por una cantidad dada
--TEST: aumentarCaudarDeltas 20 rio ->
--      Bifurcacion 100 [Pesquero]
--          (Bifurcacion 100 [] (Delta 70 [Pesquero])
--                              (Delta 45 []))
--          (Bifurcacion 125 [] (Delta 30 [Pesquero,Carguero])
--                              (Delta 25 [Petrolero]))
aumentarCaudarDeltas :: Int -> Rio -> Rio
aumentarCaudarDeltas = undefined
