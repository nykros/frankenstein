module Tarea06 where
import Tarea01(esPar)
import Tarea05(sumatoria,longitud)
{- TEMA: COMPOSICION DE FUNCIONES
   CANTIDAD DE UNDEFINEDs: 4
   Román García -}

{- NOTAS:
   * Muchas veces la solucion a un problema es una combinacion de aplicaciones
     de funciones que ya tenemos. En esta tarea NO HAY QUE HACER RECURSION. La
     recursion la hace las funciones "mas chiquitas" que nosotros llamamos.
-}

-- Dada una lista de numeros, calcular el promedio. Si la lista esta vacia
-- el promedio debe ser cero
--TEST: promedio [2,10,4,3] -> 4
promedio :: [Int] -> Int
promedio   []   = 0
promedio (x:xs) = div (sumatoria xs) (longitud xs)


-- Dada un lista de numeros, determinar la cantidad de numeros pares
--TEST: cantDePares [2,3,5,6,9] -> 2
cantDePares :: [Int] -> Int
cantDePares = undefined


-- Dada una lista de numeros, determinar cual es el mayor numero par
--TEST: mayorPar [2,3,5,6,9] -> 6
mayorPar :: [Int] -> Int
mayorPar = undefined


-- Determinar si una letra determinada aparece n veces en un String
--TEST: letraApareceNVeces 'a' 3 "abracadabra" -> False
--TEST: letraApareceNVeces 'a' 5 "abracadabra" -> True
letraApareceNVeces :: Char -> Int -> String -> Bool
letraApareceNVeces = undefined


-- Determinar si todos los numeros de la lista son pares
--TEST: sonTodosPares [2,4,5]  -> False
--TEST: sonTodosPares [2,4,10] -> True
sonTodosPares :: [Int] -> Bool
sonTodosPares = undefined
