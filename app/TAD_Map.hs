module TAD_Map (Map, emptyM, assocM, lookupM, deleteM, domM) where
import TAD_KeyValuePair
import TAD_AVL
{- Implementacion de Map basado en AVL. Asegura de O(log n) a assocM, lookupM y deleteM
   CANTIDAD DE UNDEFINEDs: 0
   Román García -}

data Map k v = M (TAVL (KeyValuePair k v))

-- Devuelve un Map vacio. O(1)
emptyM :: Ord k => Map k v
emptyM = M emptyAVL


-- Agrega al map una nueva asociacion. Si ya estaba esa clave usada, la pisa. O(log n)
assocM :: Ord k => Map k v -> k -> v -> Map k v
assocM (M tavl) k v = M (insertAVL (kvPair k v) tavl)


-- Busco una clave en el Map. Si esta devuelvo Just v, sino Nothing. O(log n)
lookupM :: Ord k => Map k v -> k -> Maybe v
lookupM (M tavl) k = val (searchAVL (kvPair k (undefined)) tavl)
  where val :: Ord k => Maybe (KeyValuePair k v) -> Maybe v
        val Nothing     = Nothing
        val (Just pair) = Just (getValue pair)


-- Borro una clave del Map. O(log n)
deleteM :: Ord k => Map k v -> k -> Map k v
deleteM (M tavl) k = M (deleteAVL (kvPair k (undefined)) tavl)


-- Devuelve todas las claves que hay almacenadas en el map. O(n)
domM :: Ord k => Map k v -> [k]
domM (M tavl) =  claves (listInOrderAVL tavl)
  where claves :: Ord k => [KeyValuePair k v] -> [k]
        claves   []   = []
        claves (p:ps) = getKey p : claves ps
