module Tarea04 where
import Tarea01(esPar)
{- TEMA: RECURSION SOBRE LISTAS CON PATRON MAP
   CANTIDAD DE UNDEFINEDs: 5
   Román García -}

{- NOTAS:
   * En las funciones que usan el patrón MAP, la cantidad de los elementos de
     entrada es la misma que de salida, pero cada uno de los elementos ha sido
     transformado de alguna forma.
   * En esta tarea, se puede usar la funcion de Haskell
       replicate :: Int -> a -> [a]
     que dado un numero y un elemento, devuelve una lista de n elementos
-}

-- Dada una lista de pares que en la primera componente tienen un numero (n) y en
-- la segunda componente una letra (l), devuelve una lista de strings, donde
-- cada string es la letra l repetida n veces.
--TEST: mapParesAString [(2,'a'),(5,'B')] -> ["aa","BBBBB"]
mapParesAString :: [(Int,Char)] -> [String]
mapParesAString     []     = []
mapParesAString ((n,l):xs) = replicate n l : mapParesAString xs


-- Dada una lista de pares de numeros, devuelve una lista con el mayor de cada par
--TEST: mapMaxDelPar [(2,8),(-7,-3),(0,-1)] -> [8,-3,0]
mapMaxDelPar :: [(Int,Int)] -> [Int]
mapMaxDelPar = undefined


-- Dada una lista de numeros, devuelve una lista de True/False que indica si
-- los numeros son pares o no
--TEST: mapEsPar [2,3,7,4,4] -> [True,False,False,True,True]
mapEsPar :: [Int] -> [Bool]
mapEsPar = undefined


-- Dada una lista de pares, devuelve una lista con las primeras componentes de cada par
--TEST: primeras [(4,3),(2,9),(9,2)] -> [4,2,9]
primeras :: [(a,b)] -> [a]
primeras = undefined


-- Dada una lista de Boolean, devuelve una lista de ceros y unos, cero si es False
-- uno si es True
--TEST: boolToBin [True,False,False,False] -> [1,0,0,0]
boolToBin :: [Bool] -> [Int]
boolToBin = undefined


-- Dada una lista de ceros y unos, devuelve una lista de False/True respectivamente
-- Parcial si la lista contiene otros numeros
--TEST: binToBool [0,1,2] -> BOOOM!
--TEST: binToBool [0,1,1] -> [False,True,True]
binToBool :: [Int] -> [Bool]
binToBool = undefined
