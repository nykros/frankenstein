module Tarea20 where
import TAD_Lista
import Tarea01 (esPar)
{- TEMA: INTRO A TIPOS DE DATOS ABSTRACTOS
   CANTIDAD DE UNDEFINEDs: 5
   Román García -}

{- NOTAS:
   * los tipos de datos abstractos introducen una separacion entre quienes
     implementan el TAD y quienes los usan:
     - Los usuarios pueden manipular los valores del TAD por medio de un conjunto
       de funciones bien definidas llamada interface. No tienen acceso a su
       representacion interna y por lo tanto no pueden hacer pattern matching.
     - Los implementadores del TAD saben como esta implementado el TAD, pueden
       usar ese conocimiento para implementar las funciones requeridas por la
       interface, pero deben asegurarse que todas las funciones cumplen con las
       invariantes de representacion que se hayan determinado.
    * En Haskell los TAD se crean poniendo cada uno en un archivo separado y
      exportando las funciones de la interface en la declaracion del modulo.
      Si queremos que persona sea un TAD debemos poner:

        module Persona (Persona, nombre, edad, crearPersona)
                          ^         ^      ^        ^
                        -----     -----------------------
                        tipo           operaciones

        data Persona = P Nombre edad

        crearPersona n e = P n e
        nombre (P n e) = n
        edad (P n e) = e

    * Notese que al exportar el tipo Persona no se exportan los constructores.
      Para exportar los constructores se debe escribir Persona(..)
    * Un TAD no tiene que ser algo complicado, sino que es algo que nos interesa
      tener separado el "como se implementa" al "como se usa"
-}

{- Implementar el tipo abstracto Lista. Una lista guarda una secuencia de
 valores del mismo tipo. A diferencia de las listas normales de Haskell,
 la operacion para saber la cantidad de elementos de la lista debe ser O(1)
 La interface de nuestra Lista tiene las siguientes funciones:
      vaciaL :: Lista a
      estaVaciaL :: Lista a -> Bool
      cabezaL :: Lista a -> a
      restoL :: Lista a  -> Lista a
      agregarL :: a -> Lista a -> Lista a
      longitudL :: Lista a -> Int

 POR UNA PROMOCIONAL ESPECIAL, EL CODIGO DE TAD LISTA está en el archivo
 TAD_Lista.hs (sin necesidad de matar undefined)
-}

-- Construyo un valor del tipo abstracto Lista Int:

-- 11:23:10:3:[]
lista = agregarL 11 (agregarL 23 (agregarL 10 (agregarL 3 vaciaL))) :: Lista Int

{-NOTA:
  Si trato de mostrar un valor de este tipo voy a obtener un mensaje de error:
     • No instance for (Show (Lista Integer)) arising from a use of ‘print’
  porque no tiene implementado como mostrarse. La decision de implementar o no
  el mostrar puede ser complicada, porque es posible mostrar detalles de la
  implementacion sin querer.

  Para nuestros ejemplos, vamos a convertir valores del TAD Lista en listas de
  Haskell, y mostrar la lista de Haskell
-}
--TEST: listar lista -> [11,23,10,3]
listarL :: Lista a -> [a]
listarL lis | esVaciaL lis = []
            | otherwise    = cabezaL lis : (listarL (restoL lis))


-- Como usuarios, implementar las siguientes funciones:

-- Devuelve solamente los numeros pares de una lista de numeros. COMPARAR CON TAREA03
--soloPares   []   = []
--soloPares (x:xs) = if esPar x
--                      then x: soloPares xs
--                      else    soloPares xs
--TEST: listarL (soloParesL lista) -> [10]
soloParesL :: Lista Int -> Lista Int
soloParesL lis | esVaciaL  lis       = vaciaL
               | esPar (cabezaL lis) = agregarL (cabezaL lis) (soloParesL (restoL lis))
               | otherwise           =                         soloParesL (restoL lis)


-- Dado una lista, elige el menor de los elementos
-- Parcial si la lista esta vacia. COMPARAR CON TAREA05:
-- menor   [x]    = x
-- menor (x:y:xs) = if x < y
--                    then menor (x:xs)
--                    else menor (y:xs)
-- Voy a usar algunas declaraciones locales (where) asi queda mas parecido
--TEST: menorL lista -> 3
menorL :: Ord a => Lista a -> a
menorL lis | longitudL lis == 1 = cabezaL lis
           | longitudL lis >= 2 = if x < y
                                    then menorL (agregarL x xs)
                                    else menorL (agregarL y xs)
  where x = cabezaL lis
        y = cabezaL (restoL lis)
        xs = restoL (restoL lis)


-- Devolver el segundo elemento de una Lista. Parcial si no tiene al menos 2 elementos
segundoElem :: Lista a -> a
segundoElem = undefined

-- Dada una lista de numeros, devuelve la suma de los mismos
--TEST: sumatoriaL lista -> 47
sumatoriaL :: Lista Int -> Int
sumatoriaL = undefined


-- Dado un elemento y una lista, indica si el elemento está en la lista
--TEST: perteneceL 3 lista -> True
--TEST: perteneceL 4 lista -> False
perteneceL :: Eq a => a -> Lista a -> Bool
perteneceL = undefined


-- Dada un numero y una lista de numeros, devuelve una lista con los numeros
-- menores al dado
--TEST: listarL (filtrarMenoresAL 10 lista) -> [3]
filtrarMenoresAL :: Int -> Lista Int -> Lista Int
filtrarMenoresAL = undefined


-- Dada una lista de pares de numeros, devuelve una lista con el mayor de cada par
--TEST: listarL (mapMaxDelParL (agregarL (4,6) vaciaL)) -> [6]
mapMaxDelParL :: Lista (Int,Int) -> Lista Int
mapMaxDelParL = undefined
