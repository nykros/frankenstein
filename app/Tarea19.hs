module Tarea19 where
import Tarea12 (Tree(..))
import Tarea13 (listarInOrder)
import Tarea14 (itemMasDerecho)
{- TEMA: ARBOLES BINARIOS DE BUSQUEDA
   CANTIDAD DE UNDEFINEDs: 3
   Román García -}

{- NOTAS:
   * Los arboles binarios de busqueda (BST en ingles) son arboles binarios
     que siempre cumplen la siguiente restriccion o INVARIANTE:

         - EmptyBST es un BST
         - (NodeBST e t1 t2) es un BST si:
            - todo elemento de t1 es menor a e
            - todo elemento de t2 es mayor a e
            - t1 y t2 son BST

       Por lo definición pasa los siguiente:
         - No hay elementos repetidos en el arbol
         - El listado inOrder del arbol produce la lista ordenada de los elementos
-}

-- Dado un BST inserta un elemento en el arbol
insertBST :: Ord a => a -> Tree a -> Tree a
insertBST e     Empty                  = Node e Empty Empty
insertBST e (Node x t1 t2) | e == x    = Node e t1 t2
                           | e < x     = Node x (insertBST e t1) t2
                           | otherwise = Node x t1 (insertBST e t2)


-- Dado un BST borrar un elemento del arbol
-- Ver video: https://www.youtube.com/watch?v=wcIRPqTR3Kc
-- Primero: Encuentro el elemento a borrar
deleteBST :: Ord a => a -> Tree a-> Tree a
deleteBST e        Empty               = Empty
deleteBST e (Node x t1 t2) | e == x    = rearmarBST t1 t2
                           | e <  x    = Node x (deleteBST e t1) t2
                           | otherwise = Node x t1 (deleteBST e t2)

-- Una vez encontrado, rearmo el arbol afectado. El caso complicado es cuando tiene
-- dos hijos, en ese caso, reemplazo el valor borrado por el valor mas grande
-- del arbol izquierdo y borro ese valor.
-- NOTA: tambien se podria usar usar el valor mas chico del arbol derecho
rearmarBST :: Ord a => Tree a -> Tree a -> Tree a
rearmarBST Empty Empty = Empty    -- No tiene hijos
rearmarBST Empty   t2  = t2          -- tiene un solo hijo
rearmarBST  t1   Empty = t1          -- tiene un solo hijo
rearmarBST  t1     t2  = Node (itemMasDerecho t1) (deleteBST (itemMasDerecho t1) t1) t2


-- Dado un BST dice si el elemento pertenece o no al arbol
perteneceBST ::  Ord a => a -> Tree a -> Bool
perteneceBST = undefined


-- Dado un BST devuelve el elemento del arbol que es igual al dado
lookupBST :: Ord a => a -> Tree a -> Maybe a
lookupBST = undefined


-- Para testear si un arbol es realmente un BST
esBST :: Ord a => Tree a -> Bool
esBST = undefined
