module Tarea02 where
{- TEMA: DEFINICION DE FUNCIONES con pattern matching
   CANTIDAD DE UNDEFINEDs: 7
   Román García -}

{- NOTAS:
   * A partir de ahora, no voy a dar en el cuerpo de las funciones los
     parametros que deben tomar, esos los tienen que poner Uds.
   * En todas las funciones (de esta tarea) se espera usen pattern matching
   * Si hay dos funciones f que hacen exactamente los mismo, las llamaremos:
     f y f' (en todas las tareas)
   * Si hay dos funciones f que hacen algo muy parecido pero no exactamente
     lo mismo para todos los casos, las llamaremos f y f2 (en todas las tareas)
   * Un String es una lista de letras:
            "Hola" == ['H','o','l','a'] == 'H':'o':'l':'a':[]
-}

-- Devuelve el tercer elemento de una lista.
-- Parcial si la lista tiene menos de tres elementos
--tercerElem [4,23,41,45,54] -> 41
tercerElem :: [a] -> a
tercerElem (a:b:c:xs) = c


-- Dado un valor booleano, devuelve su contrario. {-AKA: not -}
--TEST: negar True -> False
--TEST: negar False -> True
negar' :: Bool -> Bool
negar' = undefined


-- Dado un par de elementos devuelve el primero. {-AKA: fst -}
--TEST: primera ("Hola",45) -> "Hola"
primera :: (a, b) -> a
primera = undefined


-- Dado un par de elementos, devuelve el segundo. {-AKA: snd -}
--TEST: segunda ("Hola",45) -> 45
segunda :: (a, b) -> b
segunda = undefined


-- Dado un par de numeros enteros, devuelve la suma de los mismos
--TEST: sumaPar (23,2) -> 25
sumaPar :: (Int, Int) -> Int
sumaPar = undefined


-- Dado una lista de elementos, devuelve True si esta vacia. {-AKA: null -}
--TEST: esVacia []                 -> True
--TEST: esVacia "General Belgrano" -> False
esVacia :: [a] -> Bool
esVacia = undefined


-- Dado una lista de elementos, devuelve el primero. {-AKA: head -}
-- Parcial si la lista está vacia
--TEST: cabeza [] -> BOOOM!!
--TEST: cabeza [5,3,2] -> 5
cabeza :: [a] -> a
cabeza (x:xs) = x


-- Dado una lista de elementos, devuelve la lista sin el primero. {-AKA: abs -}
-- Parcial si la lista está vacia
--TEST: resto [] -> BOOOM!!
--TEST: resto [5,3,2] -> [3,2]
resto :: [a] -> [a]
resto = undefined


-- Dado un par que en la primera componente tiene una lista, devolver el segundo
-- elemento de la lista. Parcial si la lista tiene menos de dos elementos
--TEST: segundoDePrimera ([3,7,9,11],True) -> 7
segundoDePrimera :: ([a],b) -> a
segundoDePrimera = undefined
