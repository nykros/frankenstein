module Tarea17 where

{- TEMA: ARBOLES BINARIOS MAQUILLADOS
   CANTIDAD DE UNDEFINEDs: 4
   Román García -}

{- NOTAS:
   * A veces un arbol binario está debajo de un poco de maquillaje. Si sabemos
     trabajar con un arbol binario común, sabremos manejar este tambien.
   * Al estar dos veces la palabra Mapa a la derecha de su definicion sabemos
     que la estructura es un arbol binario, y que en vez de tener un caso base
     tiene tres casos bases.
-}

-- Se quiere representar un mapa de la siguiente forma:
data Mapa = Agua
          | Tierra
          | Cofre [Tesoro]
          | Comp Mapa Mapa  deriving Show
data Tesoro = Joya | Oro | Plata deriving Show

-- Creo un valor del tipo Mapa

mapa = Comp Agua
            (Comp (Cofre [Joya,Oro,Oro])
                  (Cofre [])
            )

-- Dado una mapa, devuelve un mapa sin joyas
--TEST: sinJoyas mapa ->
--       Comp Agua
--            (Comp (Cofre [Oro,Oro])
--                  (Cofre [])
--            )
sinJoyas :: Mapa -> Mapa
sinJoyas Agua            = Agua
sinJoyas Tierra          = Tierra
sinJoyas (Cofre tesoros) = Cofre (quitarJoyas tesoros)
sinJoyas (Comp m1 m2)    = Comp (sinJoyas m1) (sinJoyas m2)

--auxiliar: es un simple filter sobre listas. Con doble pattern matching
quitarJoyas :: [Tesoro] -> [Tesoro]
quitarJoyas     []    = []
quitarJoyas (Joya:ts) = quitarJoyas ts
quitarJoyas (t:ts)    = t:quitarJoyas ts

-- tambien puedo escribirla asi. Con simple pattern matching
quitarJoyas' :: [Tesoro] -> [Tesoro]
quitarJoyas'   []   = []
quitarJoyas' (t:ts) = if esJoya t
                         then     quitarJoyas ts
                         else t : quitarJoyas ts

-- pero necesito una funcion auxiliar
esJoya :: Tesoro -> Bool
esJoya Joya = True
esJoya  _   = False


-- Dado un mapa devuelve otro mapa donde los cofres han sido vaciados
--TEST: saquear mapa ->
--       Comp Agua
--            (Comp (Cofre [])
--                  (Cofre [])
--            )
saquear :: Mapa -> Mapa
saquear = undefined


-- Dado un mapa retorna una lista con el contenido de todos los cofres
--TEST: cofres mapa -> [Joya,Oro,Oro]
cofres :: Mapa -> [Tesoro]
cofres = undefined


-- Dado un mapa devuelve otro mapa donde todos los tesoros se le agregan Oro
-- si no tienen
--TEST: agregarOro mapa ->
--       Comp Agua
--            (Comp (Cofre [Joya,Oro,Oro])
--                  (Cofre [Oro])
--            )
agregarOro :: Mapa -> Mapa
agregarOro = undefined


-- Dado un mapa devuelve otro mapa donde las tierras han sido cambiadas por Agua
-- y las aguas por Tierra
--TEST: intercambiarTierraYAgua mapa ->
--       Comp Tierra
--            (Comp (Cofre [Joya,Oro,Oro])
--                  (Cofre [])
--            )
intercambiarTierraYAgua :: Mapa -> Mapa
intercambiarTierraYAgua = undefined
