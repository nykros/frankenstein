module Tarea21 where
import TAD_GobstonesCelda
{- TEMA: TIPOS DE DATOS ABSTRACTOS: CELDA DEL TABLERO DE GOBSTONES
   CANTIDAD DE UNDEFINEDs: 3
   Román García -}

{- NOTAS:
   * Matar los 3 undefined del archivo "TAD_GobstonesCelda.hs". La interface
     de las celdas tiene las siguientes funciones:
          celdaVacia :: Celda
          ponerC :: Color -> Celda -> Celda
          sacarC :: Color -> Celda -> Celda
          nroBolitasC :: Color -> Celda -> Int
          hayBolitasC :: Color -> Celda -> Bool
   -}

-- Creo un valor del tipo Celda con dos bolitas rojas y una negra
celda = ponerC Negro (ponerC Rojo (ponerC Rojo celdaVacia))


-- Muestra el contenido de una celda
--TEST: mostrarCelda celda -> "La celda tiene 0 azules, 1 negras, 2 rojas, 0 verdes."
mostrarCelda :: Celda -> String
mostrarCelda cel = "La celda tiene " ++
                   show (nroBolitasC Azul cel)  ++ " azules, " ++
                   show (nroBolitasC Negro cel) ++ " negras, " ++
                   show (nroBolitasC Rojo cel)  ++ " rojas, "  ++
                   show (nroBolitasC Verde cel) ++ " verdes."


-- Agrega n bolitas de un color dado a la celda. Parcial si n <0
--TEST: mostrarCelda (ponerEnCeldaN 5 Azul celda) ->
--         "La celda tiene 5 azules, 1 negras, 2 rojas, 0 verdes."
ponerEnCeldaN :: Int -> Color -> Celda -> Celda
ponerEnCeldaN n c cel | n < 0     = error "No se puede poner un cantidad negativa de bolitas"
                      | n == 0    = cel
                      | otherwise = ponerEnCeldaN (n-1) c (ponerC c cel)


--Quita de la celda n bolitas. Parcial si no hay bolitas suficientes
--TEST: mostrarCelda (ponerEnCeldaN 2 Rojo celda) ->
--         "La celda tiene 0 azules, 1 negras, 0 rojas, 0 verdes."
sacarDeCeldaN ::Int -> Color -> Celda -> Celda
sacarDeCeldaN = undefined


-- Devuelve True si hay mas de n bolitas de ese color
--TEST: nroBolitasEnCeldaMayorA Rojo 1 celda -> True
--TEST: nroBolitasEnCeldaMayorA Rojo 2 celda -> False
nroBolitasEnCeldaMayorA :: Color -> Int -> Celda -> Bool
nroBolitasEnCeldaMayorA = undefined


-- Indica si existe al menos una bolita de cada color posible
--TEST: celdaTieneBolitasDeCadaColor celda -> False
celdaTieneBolitasDeCadaColor :: Celda -> Bool
celdaTieneBolitasDeCadaColor = undefined
