module TAD_AVL (TAVL, emptyAVL, insertAVL, deleteAVL, searchAVL, listInOrderAVL) where
{- Basado en la version dada en clase de Federico Arrambari
   Es un ADT porque no permito tocar la implementacion, que no es compatible
   con los arboles "normales" que veniamos haciendo porque tiene mas info
   CANTIDAD DE UNDEFINEDs: 0
   Román García -}

data TAVL a = ET | Node Int (TAVL a) a (TAVL a) deriving (Show)

-- Crea un nuevo TAVL
emptyAVL:: Ord a => TAVL a
emptyAVL = ET

-- Inserta un elemento en el AVL
insertAVL :: Ord a => a -> TAVL a -> TAVL a
insertAVL v ET = Node 1 ET v ET
insertAVL v (Node n ti vn td) | v > vn = level (Node n ti vn (insertAVL v td))
                              | v < vn = level (Node n (insertAVL v ti) vn td)
                              | otherwise = Node n ti v td


-- funcion auxiliar: Calcula la altura en base a la altura de los hijos
depth :: TAVL a -> TAVL a -> Int
depth a b = 1 + max (heigth a) (heigth b)

-- funcion auxiliar: calcula la altura del arbol
heigth :: TAVL a -> Int
heigth ET = 0
heigth (Node h _ _ _) = h


-- Left Left Case
--          v                  lv
--        /   \              /   \
--     lv      r   =>    ll       v
--    /   \                     /   \
--  ll    lr                   lr     r

-- funcion auxiliar: reacomoda el arbol. Hace todo el trabajo pesado
level :: TAVL a -> TAVL a
level ET = ET
--Left Left Case
level (Node h (Node lh ll lv lr ) v r)
    | (lh -(heigth r)) > 1 && (heigth ll - heigth lr) > 0 = Node lh ll lv (Node (depth r lr) lr v r)
--Right Right Case
level (Node h l v (Node rh rl rv rr))
    | (rh - heigth l) > 1 && (heigth rr - heigth rl ) > 0 = Node rh (Node (depth l  rl) l v rl) rv rr
--Left Right Case
level (Node h (Node lh ll lv (Node rh rl rv rr)) v r )
    | (lh - heigth r) > 1 = Node h (Node (rh +1) (Node (lh-1) ll lv rl) rv rr) v r
--Right Left Case
level (Node h l v (Node rh (Node lh ll lv lr) rv rr))
    | (rh - heigth l) > 1  = Node h l v (Node (lh+1) ll lv (Node (rh-1) lr rv rr))
-- Re-weighting
level (Node h l v r) = let (l_, r_) = (level l, level r)
                           in Node (depth l_ r_) l_ v r_

-- Borra un elemento del arbol
deleteAVL ::Ord a =>  a -> TAVL a -> TAVL a
deleteAVL _ ET = ET
deleteAVL x (Node h left v right) | x > v = level (Node h left v (deleteAVL x right))
                                  | x < v = level (Node h (deleteAVL x left) v right)
                                  | x == v = level (rebuild left right)

--funcion auxiliar: recompone el arbol que se le ha borrado la raiz
rebuild ::Ord a => TAVL a -> TAVL a -> TAVL a
rebuild ET y = y
rebuild x ET = x
rebuild x y = Node (depth xsinE y) xsinE (maxAVL x) y
              where xsinE = deleteAVL (maxAVL x) x

-- Busco el maximo, que el el nodo mas a la derecha
maxAVL :: Ord a => TAVL a -> a
maxAVL (Node _ _ v ET) = v
maxAVL (Node _ l v r) = maxAVL r


-- Busco un item en el arbol
searchAVL::Ord a => a -> TAVL a -> Maybe a
searchAVL v ET = Nothing
searchAVL v (Node n ti vn td) | v > vn    = searchAVL v td
                              | v < vn    = searchAVL v ti
                              | otherwise = Just vn


-- Devuelvo los elementos del arbol listados inOrder, o sea quedan ordenados
listInOrderAVL :: Ord a => TAVL a -> [a]
listInOrderAVL ET = []
listInOrderAVL (Node n ti vn td) = listInOrderAVL ti ++ [vn] ++ listInOrderAVL td
