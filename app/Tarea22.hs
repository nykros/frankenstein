module Tarea22 where
import TAD_Cola
{- TEMA: TIPOS DE DATOS ABSTRACTOS: COLAS
   CANTIDAD DE UNDEFINEDs: 2
   Román García -}

{- NOTAS:
   * El archivo TAD_Cola.hs contiene una implementacion de cola que usa
     lista. Hay que matar 4 undefineds.
   * La interface de las colas tiene las siguientes funciones:
          emptyQ :: Queue a
          isEmptyQ :: Queue a -> Bool
          enqueueQ :: a -> Queue a -> Queue a
          firstQ :: Queue a -> a
          dequeueQ :: Queue a -> Queue a
   -}

-- Defino valores del tipo Queue
queue1 = enqueueQ 3 (enqueueQ 2 (enqueueQ 1 emptyQ))
queue2 = enqueueQ 7 (enqueueQ 8 emptyQ)

--TEST: mostrarCola queue1 -> "SALE 1<2<3< ENTRA"
--TEST: mostrarCola queue2 -> "SALE 8<7< ENTRA"
mostrarCola :: Show a => Queue a -> String
mostrarCola q | isEmptyQ q = "La cola esta vacia"
              | otherwise  = "SALE "++ (mostrarColaRec q) ++" ENTRA"

--funcion auxiliar: Hace el recorrido recursivo de la cola
mostrarColaRec :: Show a => Queue a -> String
mostrarColaRec q | isEmptyQ q = ""
                 | otherwise  = show (firstQ q) ++"<"++mostrarColaRec(dequeueQ q)


-- Inserta todos los elementos de la segunda cola en la primera
--TEST: mostrarCola (unirColas queue1 queue2) -> "SALE 1<2<3<8<7< ENTRA"
unirColas :: Queue a -> Queue a -> Queue a
unirColas q1 q2 | isEmptyQ q2 = q1
            | otherwise   = unirColas (enqueueQ (firstQ q2) q1) (dequeueQ q2)


-- Cuenta la cantidad de elementos de la cola
--TEST: longitudCola queue1 -> 3
longitudQ :: Queue a -> Int
longitudQ = undefined


-- Pone todos los elementos de la cola en una lista. El primero de la lista
-- debe ser el primero en salir de la cola, el segundo de la lista deber ser
-- el segundo en salir de la cola, y asi sucesivamente
--TEST: listarQ queue1 -> [1,2,3]
listarQ :: Queue a -> [a]
listarQ = undefined


-- Devuelve los primeros n elementos de la cola en una lista. Suponer que hay al
-- menos n elementos en la cola. El orden debe ser el de salida de la cola
{-NOTA: la recursion se hace sobre el numero -}
--TEST primerosLaCola 2 queue1 -> [1,2]
primerosDeCola :: Int -> Queue a -> [a]
primerosDeCola = undefined
