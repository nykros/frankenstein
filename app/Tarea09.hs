module Tarea09 where
{- TEMA: TIPOS DE DATOS ALGEBRAICOS. EL TIPO PERSONA DE LA PRACTICA 2
   CANTIDAD DE UNDEFINEDs: 4
   Román García -}

data Persona = Persona String Int deriving (Show,Eq)

-- defino algunos valores de Persona para tests
juan  = Persona "Juan" 55
pedro = Persona "Pedro" 12
pablo = Persona "Pablo" 33
jose  = Persona "Jose" 10
personas = [juan,pedro,pablo,jose]

----- Defino los proyectores: son las "inversas" de los constructores ------
--TEST: nombre jose -> "Jose"
--TEST: edad pedro -> 12
nombre :: Persona -> String
nombre (Persona n _) = n

edad :: Persona -> Int
edad = undefined

----- Definir las siguiente funciones -----

-- Dada una persona la devuelve con la edad aumentada en 1
--TEST: crecer juan -> Persona "Juan" 56
crecer :: Persona -> Persona
crecer (Persona n e) = Persona n (e+1)


-- Dados un nombre y una persona, reemplaza el nombre de la persona por el dado
--TEST: cambioDeNombre "Juan Jose" juan -> Persona "Juan Jose" 55
cambioDeNombre :: String -> Persona -> Persona
cambioDeNombre = undefined


-- Dadas dos personas, determina si la primera es menor que la segunda
--TEST: esMenorQueLaOtra pedro pablo -> True
--TEST: esMenorQueLaOtra juan jose -> False
esMenorQueLaOtra :: Persona -> Persona -> Bool
esMenorQueLaOtra = undefined


-- Dada un persona y una lista de personas, devuelve la lista de personas que
-- son menores que la dada
--TEST: menoresQue juan  personas -> []
--TEST: menoresQue pedro personas -> [Persona "Jose" 10]
menoresQue :: Persona -> [Persona] -> [Persona]
menoresQue = undefined


-- Calcula el promedio de las edades de una lista de personas
--TEST: promedioEdades personas -> 27
promedioEdades ::[Persona] -> Int
promedioEdades = undefined


-- OPCIONAL: En una sociedad antigua, los votos valian mas con la edad:
--  Los de mas 50 valian 10
--  Los de 30 a 50 valian 5
--  los de 18 a 29 valian 1
-- Determinar la cantidad de votos que puede emitir una lista de personas
--TEST: cantDeVotos personas -> 15
cantDeVotos :: [Persona] -> Int
cantDeVotos = undefined
