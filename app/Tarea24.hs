module Tarea24 where
import TAD_Conjunto
import Tarea12 (Tree(..))
import Tarea07 (ordenar,sinUltimo)
{- TEMA: TIPOS DE DATOS ABSTRACTOS: CONJUNTOS
   CANTIDAD DE UNDEFINEDs: 5
   Román García -}

{- NOTAS:
   * Matar los 6 undefined del archivo "TAD_Conjunto.hs"
   * La interface de conjunto contiene las siguientes funciones:
         vacioC :: Conjunto a
         agregarC :: Eq a => a -> Conjunto a -> Conjunto a
         perteneceC :: Eq a => a -> Conjunto a -> Bool
         cantidadC :: Eq a => Conjunto a -> Int
         borrarC :: Eq a => a -> Conjunto a -> Conjunto a
         unionC :: Eq a => Conjunto a -> Conjunto a -> Conjunto a
         listarC :: Eq a => Conjunto a -> [a]
-}


conj1 = agregarC 1 (agregarC 7 (agregarC 3 (agregarC 4 vacioC))) -- {1,3,4,7}
conj2 = agregarC 3 (agregarC 5 (agregarC 9 vacioC))              -- {3,5,9}

--Suponiendo que los elementos de un conjunto tienen orden, muestra un Conjunto
--OJO!! La restrición de orden no es parte del TAD_Conjunto, sino de esta funcion
-- a fin de que cada conjunto tenga una unica representacion como string
--TEST: mostrarConjunto conj1 -> "{1,3,4,7}"
--TEST: mostrarConjunto conj2 -> "{3,5,9}"
mostrarConjunto :: (Ord a, Show a) => Conjunto a -> String
mostrarConjunto s = "{" ++ tail(sinUltimo(show (ordenar (listarC s)))) ++ "}"

-- Dados una lista y un conjunto, devuelve una lista con todos los elementos
-- que pertenecen al conjunto
--TEST: losQuePertenecen [1..5] conj1 -> [1,3,4]
losQuePertenecen :: Eq a => [a] -> Conjunto a -> [a]
losQuePertenecen   []   s = []
losQuePertenecen (x:xs) s = if x `perteneceC` s
                               then x:losQuePertenecen xs s
                               else losQuePertenecen xs s


-- Dada una lista, pone todos los elementos de la lista en el conjunto
--TEST: mostrarConjunto (ponerEnConjunto [1..5]) -> "{1,2,3,4,5}"
ponerEnConjunto :: Eq a => [a] -> Conjunto a
ponerEnConjunto [] = vacioC
ponerEnConjunto (x:xs) = agregarC x (ponerEnConjunto xs)


-- Quita todos los elementos repetidos de la lista dada utilizando un Conjunto
-- como estructura auxiliar
--TEST: sinRepetidos [1,4,1,5,1,4,3,5] -> [1,4,3,5]
sinRepetidos :: Eq a => [a] -> [a]
sinRepetidos = undefined


--Dado una lista de conjuntos devuelve un conjunto con la union de
--todos los conjuntos del arbol
--TEST: mostrarConjunto (unirTodos [conj1,conj2]) -> "{1,3,4,5,7,9}"
unirTodos :: Eq a => [Conjunto a] -> Conjunto a
unirTodos = undefined


-- Dado dos conjuntos, devuelve un conjunto con los elementos que estan en ambos
--TEST: mostrarConjunto (intersectar con1 conj2) -> "{3}"
intersectar :: Eq a => Conjunto a -> Conjunto a -> Conjunto a
intersectar = undefined


--Dado una lista de conjuntos devuelve un conjunto con la interseccion de
--todos los conjuntos del arbol
--TEST: mostrarConjunto (intersectarTodos [con1,conj2]) -> "{3}"
intersectarTodos :: Eq a => [Conjunto a] -> Conjunto a
intersectarTodos = undefined


--Dado un arbol de conjuntos devuelve un conjunto con la interseccion de
--todos los conjuntos del arbol
--TEST: mostrarConjunto(intersectarTodosEnArbol Empty) -> "{}"
intersectarTodosEnArbol :: Eq a => Tree (Conjunto a) -> Conjunto a
intersectarTodosEnArbol = undefined
