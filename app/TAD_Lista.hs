module TAD_Lista(Lista,vaciaL,esVaciaL,cabezaL,restoL,agregarL,longitudL) where
{- TIPO DE DATOS ABSTRACTO LISTA: Parte de Tarea20.hs -
   CANTIDAD DE UNDEFINEDs: 0
   Román García -}

-- Implementar el tipo abstracto Lista. Una lista guarda una secuencia de
-- valores del mismo tipo. A diferencia de las listas normales de Haskell,
-- la operacion para saber la cantidad de elementos de la lista debe ser O(1)

data Lista a = MkLista [a] Int

-- La interface de Lista es:

-- Devuelve una lista sin elementos
vaciaL :: Lista a
vaciaL = MkLista [] 0


-- Dado una lista de elementos, devuelve True si esta vacia.
esVaciaL :: Lista a -> Bool
esVaciaL (MkLista xs n) = n == 0


-- Dado una lista de elementos, devuelve el primero. Parcial si la lista está vacia
cabezaL :: Lista a -> a
cabezaL (MkLista (x:xs) n) = x


-- Dado una lista de elementos, devuelve la lista sin el primero. Parcial si la lista está vacia
restoL :: Lista a  -> Lista a
restoL (MkLista (x:xs) n) = MkLista xs (n-1)


-- Agrega un elemento a la lista al principio de la misma
agregarL :: a -> Lista a -> Lista a
agregarL x (MkLista xs n) = MkLista (x:xs) (n+1)


-- Dada una lista de elementos, devuelve la cantidad de elementos que contiene
longitudL :: Lista a -> Int
longitudL (MkLista xs n) = n
