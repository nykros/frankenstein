module TAD_Cola (Queue,emptyQ,isEmptyQ,dequeueQ,enqueueQ,firstQ) where
{- Implementacion de Colas con listas parecida a la interface de la Practica 4
   CANTIDAD DE UNDEFINEDs: 4
   Román García -}

data Queue a = Q [a]

-- Crea una cola vacia
emptyQ :: Queue a
emptyQ = undefined

--Dada una cola indica si la cola está vacia
isEmptyQ :: Queue a -> Bool
isEmptyQ = undefined

-- Dados un elemento y una cola, agrega ese elemento a la cola
enqueueQ :: a -> Queue a -> Queue a
enqueueQ a (Q xs) = Q (agregarAlFinal a xs)

-- funcion auxiliar: agrego al final de la lista
agregarAlFinal :: a -> [a] -> [a]
agregarAlFinal a   []   = [a]
agregarAlFinal a (x:xs) = x: agregarAlFinal a xs

-- Dada una cola, devuelve el primer elemento de la cola
firstQ :: Queue a -> a
firstQ = undefined

-- Dada una cola, la devuelve sin su primer elemento
dequeueQ :: Queue a -> Queue a
dequeueQ = undefined
