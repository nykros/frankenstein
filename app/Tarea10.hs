module Tarea10 where
{- TEMA: TIPOS DE DATOS ALGEBRAICOS. AUTOS
   CANTIDAD DE UNDEFINEDs: 3 + 7
   Román García -}

{- NOTAS:
   * Ahora les toca a ustedes definir el tipo algebraico
   * Definir algunos valores de tipo Auto y una lista con autos
   * Hacer pruebas con los valores definidos
-}

{- Definir el tipo de datos MARCA con las alternativas Ford,
   Fiat, Chevrolet, Toyota; el COMBUSTIBLE puede ser nafta, gasoil, gas.
   Para este ejercicio, de un AUTO nos interesa saber la marca, el combustible
   que usa, su velocidad maxima y peso. -}

data Marca       --  <ACA PONER SU DEFINICION>
data Combustible --  <ACA PONER SU DEFINICION>
data Auto        --  <ACA PONER SU DEFINICION>
type Velocidad   = Double
type Peso        = Double

--Definir las funciones de acceso:

marca :: Auto -> Marca
marca = undefined

combustible :: Auto -> Combustible
combustible = undefined

velocidadMaxima :: Auto -> Velocidad
velocidadMaxima = undefined

peso  :: Auto -> Peso
peso = undefined


-- Ejercicio 3: Definir las siguientes funciones:

-- Dada una lista de autos, se queda solamente con los que tengan
-- un peso entre los dos dados, inclusive
filtrarPorPeso :: Peso -> Peso -> [Auto] -> [Auto]
filtrarPorPeso peso1 peso2   []   = []
filtrarPorPeso peso1 peso2 (a:as) = if pesoAutoEstaEntre a peso1 peso2
                                       then a : filtrarPorPeso peso1 peso2 as
                                       else     filtrarPorPeso peso1 peso2 as

--auxiliar: LAS FUNCIONES AUXILIARES SON MIS AMIGAS
pesoAutoEstaEntre :: Auto -> Peso -> Peso -> Bool
pesoAutoEstaEntre auto peso1 peso2 = peso auto >= peso1 &&
                                     peso auto <= peso2


-- Dada una lista de autos, se queda solamente con los que usen
-- un determinado tipo de combustible
filtrarPorCombustible :: Combustible -> [Auto] -> [Auto]
filtrarPorCombustible = undefined


-- Devuelve la cantidad de autos a nafta de la lista
cantAutosANafta :: [Auto] -> Int
cantAutosANafta = undefined


-- Determinar si el auto más eficiente de una lista de autos
-- puede ir a mas de 130 km/h
elMasEficienteEsRapido :: [Auto] -> Bool
elMasEficienteEsRapido = undefined
