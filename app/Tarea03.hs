module Tarea03 where
import Tarea01(esPar)
{- TEMA: RECURSION SOBRE LISTAS CON PATRON FILTER
   CANTIDAD DE UNDEFINEDs: 6
   Román García -}

{- NOTAS:
   * REUSAR, SIEMPRE REUSAR. Si faltan los import en la tarear: AGREGARLOS
   * En las funciones que usan el patrón FILTER, la cantidad de los elementos
     de la lista resultante puede variar, pero los elementos son los mismos
     que en la lista de entrada. Un filter simplemente selecciona cuales items
     van a ser parte del resultado y cuales no. Los que van a ser filtrados, o
     sea, van a ser "quitados de la lista" son los que no son seleccionados
     para quedarse en la lista resultante.
-}

-- Devuelve los numeros pares de una lista de numeros
--TEST: soloPares [3,5,2,7,-8] -> [2,-8]
soloPares :: [Int] -> [Int]
soloPares   []   = []
soloPares (x:xs) = if esPar x
                      then x: soloPares xs
                      else    soloPares xs


-- Dado un numero y una lista de numeros, devuelve una lista con los numeros
-- menores al dado
--TEST: menoresQue 5 [3,5,2,7,-8] -> [3,2,-8]
menoresQue :: Ord a => a -> [a] -> [a]
menoresQue y   []   = []
menoresQue y (x:xs) = if x < y
                         then x : menoresQue y xs
                         else menoresQue y xs


-- Dado un numero y una lista de numeros, devuelve una lista con los numeros
-- mayores o iguales al dado
--TEST: mayoresOIgualesQue 5 [3,5,2,7,-8] -> [5,7]
mayoresOIgualesQue :: Ord a => a -> [a] -> [a]
mayoresOIgualesQue = undefined


-- Dada un elemento y una lista, saca los elementos iguales al dado
--TEST: sacarIgualesA 'I' "MISSISSIPPI" -> "MSSSSPP"
sacarIgualesA :: Eq a => a -> [a] -> [a]
sacarIgualesA = undefined


-- Dada una lista de pares de enteros y un entero, devuelve los pares cuyas dos
-- componentes sean mayores al numero dado
--TEST: paresMayoresA [(2,7),(4,5),(3,1)] 3 -> [(4,5)]
paresMayoresA :: [(Int,Int)] -> Int ->  [(Int,Int)]
paresMayoresA = undefined


--Dada una lista de pares, devuelve aquellos cuyas componentes sean iguales
--TEST: soloParesIguales [(1,3),(2,2),(5,2)] -> [(2,2)]
soloParesIguales :: Eq a => [(a,a)] -> [(a,a)]
soloParesIguales = undefined

-- Dada una lista que contiene listas, devolver la lista que contiene listas
-- no vacias
--TEST: filtrarListasVacias [[1],[],[4,2],[]] -> [[1],[4,2]]
filtrarListasVacias :: [[a]] -> [[a]]
filtrarListasVacias = undefined


-- Dada una lista de Strings, devolver los strings que empiezan con la letra 'A'
--TEST: empiezanConA ["Avanza","a","la","Alacena"] -> ["Avanza","Alacena"]
empiezanConA :: [String] -> [String]
empiezanConA = undefined
