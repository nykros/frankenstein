module TAD_KeyValuePair(KeyValuePair,kvPair,getKey,getValue) where
{- Implementa un par clave-valor que se ordena y se verifica la igualdod por la
   clave. Es util como parte de otras estructuras de datos
   CANTIDAD DE UNDEFINEDs: 0
   Román García -}

data KeyValuePair k v = KV (k,v)

-- Construye un par clave-valor
kvPair::Ord k => k -> v ->KeyValuePair k v
kvPair k v = KV (k,v)

-- Obtiene la clave del par
getKey:: Ord k => KeyValuePair k v -> k
getKey (KV (k,v)) = k

-- Obtiene el valor del par
getValue:: Ord k=> KeyValuePair k v -> v
getValue (KV (k,v)) = v

-- Define que == compare solamente las claves
instance Eq k => Eq (KeyValuePair k v) where
  KV (k1,v1) == KV (k2,v2) = (k1==k2)

-- Ordena los pares por la primera componente
instance Ord k => Ord (KeyValuePair k v) where
  KV (k1,v1) <= KV (k2,v2) = (k1<=k2)
