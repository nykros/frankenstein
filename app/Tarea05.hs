module Tarea05 where
{- TEMA: RECURSION SOBRE LISTAS CON PATRON FOLD
   CANTIDAD DE UNDEFINEDs: 7
   Román García -}

{- NOTAS:
   * En las funciones que usan el patrón FOLD (plegar, como haciendo un origami),
     la lista es reducida a un solo valor, ya sea un maximo, minino o alguna otra
     operacion. Muchas veces no es siquiera necesario recorrer toda la lista.
     Este patron es tan poderoso y generico que map y filter son casos particulares
     de fold.
-}

-- Dado una lista, elige el menor de los elementos. {-AKA: minimum -}
-- Parcial si la lista esta vacia
--TEST: masChico [] -> BOOOM!
--TEST: masChico [5,3,8,4] -> 3
masChico :: Ord a => [a] -> a
masChico   [x]    = x
masChico (x:y:xs) = if x < y
                   then masChico (x:xs)
                   else masChico (y:xs)



-- Dada una lista de numeros, devuelve la suma de los mismos. {-AKA: sum -}
--TEST: sumatoria [2,8,5] -> 15
sumatoria :: [Int] -> Int
sumatoria = undefined


-- Dada una lista de elementos, devuelve la cantidad. {-AKA: lenght -}
--TEST: longitud "Villanueva" -> 10
longitud :: [a] -> Int
longitud = undefined


-- Dado un elemento y una lista, indica la cantidad de veces que esta en la lista
--TEST: apariciones 'I' "MISSISSIPPI" -> 4
apariciones :: Eq a => a -> [a] -> Int
apariciones = undefined


-- Dada una lista de booleanos, indicar si todos ellos son True
--TEST: todoVerdad [] -> True
--TEST: todoVerdad [True,True] -> True
--TEST: todoVerdad [True,True,False] -> False
todoVerdad :: [Bool] -> Bool
todoVerdad = undefined


-- Dada una lista de booleanos, indica si alguno de ellos es True
--TEST: algunaVerdad [] -> False
--TEST: algunaVerdad [True,True] -> True
--TEST: algunaVerdad [True,True,False] -> True
algunaVerdad :: [Bool] -> Bool
algunaVerdad = undefined


-- Dado un elemento y una lista, indica si el elemento está en la lista. {-AKA: elem -}
--TEST: pertenece 2 [3,5,24] -> False
--TEST: pertenece 5 [3,5,24] -> True
pertenece :: Eq a => a -> [a] -> Bool
pertenece = undefined


-- Dado una lista, elige el mayor de los elementos. {-AKA: maximum -}
-- Parcial si la lista esta vacia
--TEST: masGrande [] -> BOOOM!
--TEST: masGrande [5,3,8,4] -> 8
masGrande :: Ord a => [a] -> a
masGrande = undefined
