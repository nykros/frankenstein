module TAD_GobstonesCelda (Celda, Color(..), celdaVacia, ponerC, sacarC, nroBolitasC, hayBolitasC) where
{- Vamos a implementar una Celda de Gobstones de la forma mas pavota posible.
   La interface es (salvo algunos renombres) la misma de la practica 5
   CANTIDAD DE UNDEFINEDs: 3
   Román García -}

data Color = Azul | Negro | Rojo | Verde deriving (Eq,Ord,Show)
data Celda = MkCelda Int Int Int Int

-- Crea una celdacon cero bolitas de cada color
celdaVacia :: Celda
celdaVacia = MkCelda 0 0 0 0


-- Agrega una bolita de ese color a la celda
ponerC :: Color -> Celda -> Celda
ponerC Azul  (MkCelda a n r v) = MkCelda (a+1) n r v
ponerC Negro (MkCelda a n r v) = MkCelda a (n+1) r v
ponerC Rojo  (MkCelda a n r v) = MkCelda a n (r+1) v
ponerC Verde (MkCelda a n r v) = MkCelda a n r (v+1)


-- Saca una bolita de ese color, parcial cuando no hay bolitas de ese color
sacarC :: Color -> Celda -> Celda
sacarC = undefined


-- Devuelve la cantidad de bolitas de ese color
nroBolitasC :: Color -> Celda -> Int
nroBolitasC = undefined


-- Indica si hay bolitas de ese color
hayBolitasC :: Color -> Celda -> Bool
hayBolitasC = undefined
