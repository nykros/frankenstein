module Tarea26 where
import TAD_Map
import TAD_GobstonesCelda
import TAD_GobstonesMinitablero
import Data.Maybe
{- TEMA: TIPOS DE DATOS ABSTRACTOS: MINITABLERO DE GOBSTONES
   CANTIDAD DE UNDEFINEDs: 4
   Román García -}

{- NOTAS:
   * Matar los 4 undefined del archivo "TAD_GobstonesMinitablero.hs"
-}

-- Dada una direccion "d", mueve el cabezal hacia de borde "d"
irAlExtremo :: Dir -> MiniTablero -> MiniTablero
irAlExtremo dir tablero | puedeMover dir tablero = irAlExtremo dir (mover dir tablero)
                        | otherwise              = tablero


-- Pone "n" bolitas de cada color en todas las celdas de la fila
llenarHasta :: Int -> MiniTablero -> MiniTablero
llenarHasta = undefined


-- Dado un tablero indica cuatas celdas posee
contarCeldas :: MiniTablero -> Int
contarCeldas = undefined


-- Dado un tablero indica si todas sus celdas se encuentran vacias
noHayBolitas :: MiniTablero -> Bool
noHayBolitas = undefined


-- Dado un tablero arma un Map en donde indica para cada color cuantas
-- bolitas hay en total del tablero
cantidadesDeBolitas :: MiniTablero -> Map Color Int
cantidadesDeBolitas = undefined
