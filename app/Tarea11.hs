module Tarea11 where
{- TEMA: TIPOS DE DATOS ALGEBRAICOS. PRIMER PARCIAL
   CANTIDAD DE UNDEFINEDs: 3 + algunas funciones de acceso
   Román García -}

{- NOTAS:
   * Siempre que tengamos que hacer recursion sobre un tipo algebraico que
     contenga una lista, conviene hacer una funcion auxiliar que se encarga
     de la recursion con la lista. La funcion principal se debe encargar
     simplemente de "destapar", computar y "tapar"
   * La cantidad de funciones de acceso en gral se pueden calcular como
      una por cada opcion de un tipo suma (los que llevan "|") y una por cada
      componente de un tipo multiplicacion (por ejemplo Carta)
-}

-- Definir los tipos de dato Palo, con las alternativas corazon,
-- diamante, pica y trebol. Carta, como un palo y un numero (un renombre de Int)
-- y Mazo, como una lista de cartas.

data Palo   = Corazon | Diamante | Pica | Trebol deriving (Eq,Show)
data Carta  = MkCarta Palo Numero deriving (Show)
type Numero = Int
data Mazo   = MkMazo [Carta] deriving (Show)


-- Definimos algunos elementos de esos tipos
carta1 = MkCarta Corazon 3
carta2 = MkCarta Trebol 5
carta3 = MkCarta Corazon 10
mazo   = MkMazo [carta1, carta2, carta3]

-- Definir las funciones de acceso (PISTA: SON al menos seis)

esCorazon :: Palo -> Bool
esCorazon Corazon = True
esCorazon   _     = False

palo :: Carta -> Palo
palo (MkCarta p _) = p

numero :: Carta -> Numero
numero (MkCarta _ n) = n


-- Definir las siguientes funciones utilizando Pattern Matching

-- Dados dos palos indica si el primero es mayor que el segundo.
-- El orden es: Corazon > Pica > Diamante > Trebol
--TEST: esMayorPalo Pica Corazon -> False
--TEST: esMayorPalo Pica Trebol -> True
esMayorPalo :: Palo -> Palo -> Bool
esMayorPalo Corazon  Corazon = False
esMayorPalo Corazon    _     = True
esMayorPalo Pica     Corazon = False
esMayorPalo Pica     Pica    = False
esMayorPalo Pica       _     = True
esMayorPalo Diamante Trebol  = True
esMayorPalo Diamante   _     = False
esMayorPalo Trebol     _     = False


-- Dadas dos cartas indica si la primera es mayor que la segunda. Una carta es
-- mayor que otra si su numero es mayor, desempatando por palo si es necesario.
--TEST: esMayorCarta carta1 carta2 -> MkCarta Trebol 5
esMayorCarta :: Carta -> Carta -> Bool
esMayorCarta = undefined


--Dado un mazo se queda unicamente con las cartas del palo pedido
--TEST: filtrarPalo Corazon mazo -> MkMazo [MkCarta Corazon 3,MkCarta Corazon 10]
filtrarPalo:: Palo -> Mazo -> Mazo
filtrarPalo p (MkMazo cs) = MkMazo (filtrar p cs)

--auxiliar: ESTA ES LA QUE SE ENCARGA DE HACER LA RECURSION
filtrar :: Palo -> [Carta] -> [Carta]
filtrar p [] = []
filtrar p (c:cs) = if (palo c == p)
                      then c : filtrar p cs
                      else     filtrar p cs


-- Dado un mazo devuelve la mayor carta en el
--TEST: mayorCartaDelMazo mazo -> MkCarta Corazon 10
mayorCartaDelMazo :: Mazo -> Carta
mayorCartaDelMazo = undefined


-- Dada un mazo devuelve la lista con los numeros de las cartas en el
-- TEST: numerosDelMazo mazo -> [3,5,10]
numerosDelMazo :: Mazo -> [Numero]
numerosDelMazo = undefined
