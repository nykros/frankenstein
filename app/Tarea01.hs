module Tarea01 where
{- TEMA: DEFINICION DE FUNCIONES sin pattern matching
   CANTIDAD DE UNDEFINEDs: 6
   Román García -}

{- NOTAS:
   * Siempre hay que reemplazar el "undefined" por la definicion que corresponda
   * Los test sirven tanto para chequear que nuestras soluciones funcionan
     como documentacion para aclarar que es lo que la funcion deberia hacer
   * Los numeros negativos en Haskell hay que escribirlos entre parentesis
     (sino Haskell espera que ¨-" sea un operador binario)
-}

-- Devuelve el valor absoluto del numero dado. {-AKA: abs -}
--TEST: valorAbsoluto 45     -> 45
--TEST: valorAbsoluto (-342) -> 342
valorAbsoluto :: Int -> Int
valorAbsoluto n = if n >= 0
                     then n
                     else (-n)


-- Devuelve True si el numero es par {-AKA: even -}
--TEST: esPar 7 -> False
--TEST: esPar 8 -> True
esPar :: Int -> Bool
esPar n = n `mod` 2 == 0


-- Dado un numero entero devuelve su sucesor {-AKA: succ -}
--TEST: sucesor 78 -> 79
sucesor :: Int -> Int
sucesor = undefined


-- Dado dos numeros enteros devuelve la suma de ambos
--TEST: suma 76 4 -> 80
suma :: Int -> Int -> Int
suma n m = undefined


-- Devuelve True si es mayor o igual a cero
--TEST: esPositivo 3 -> TRUE
esPositivo :: Int -> Bool
esPositivo n = undefined


-- Dados dos numeros, devuelve el que es el mayor de los dos {-AKA: max -}
--TEST: maximo 43 13 -> 43
maximo :: Int -> Int -> Int
maximo n m = undefined


-- Dados dos numeros, devuelve el que es menor de los dos {-AKA: min -}
--TEST: minimo 43 13 -> 13
minimo :: Int -> Int -> Int
minimo n m = undefined


-- Dado un valor booleano, devuelve su contrario {-AKA: not -}
--TEST: negar True -> False
--TEST: negar False -> True
negar :: Bool -> Bool
negar b = undefined
