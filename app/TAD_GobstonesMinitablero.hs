module TAD_GobstonesMinitablero where
import TAD_GobstonesCelda
import TAD_Map
import Data.Maybe
{- Vamos a implementar el minitablero de la practica 5 usando Map.
   CANTIDAD DE UNDEFINEDs: 4
   Román García -}

data MiniTablero = MkT (Map Coord Celda) Coord
type Coord = Int
data Dir = Este | Oeste deriving (Show,Eq)

-- Crea una fila de celdas vacias de tamaño n, apuntado a la
-- primera celda
crearFila :: Int ->  MiniTablero
crearFila n | n > 0     = MkT (llenarFila (n-1)) 0
            | otherwise = error "BOOM!! El tamaño del tablero debe ser mayor que cero"

--funcion auxiliar
llenarFila :: Int -> Map Coord Celda
llenarFila 0 = assocM emptyM 0 celdaVacia
llenarFila n = assocM (llenarFila (n-1)) n celdaVacia


-- Dada una direccion d, mueve el cabezas hacia d. Esta funcion
-- es parcial cuando no existe una celda en esa direccion
mover :: Dir -> MiniTablero -> MiniTablero
mover = undefined


--Dada una direccion indica si existe una celda en esa direccion
puedeMover :: Dir -> MiniTablero -> Bool
puedeMover dir (MkT m cab) = isJust (lookupM m (deltaDir dir cab))

--funcion auxiliar
deltaDir :: Dir -> Int -> Int
deltaDir Este  n = n+1
deltaDir Oeste n = n-1


--Poner una bolita de la celda actual del color indicado
poner :: Color -> MiniTablero -> MiniTablero
poner c (MkT m cab) = MkT (assocM m cab (ponerC c (celdaEn m cab))) cab

--funcion auxiliar
celdaEn :: Map Coord Celda -> Coord -> Celda
celdaEn m coord = fromJust (lookupM m coord)


--Saca una bolita de la celda actual del color indicado
sacar :: Color -> MiniTablero -> MiniTablero
sacar = undefined


-- Devuelve el número de bolitas de un color en la celda actual
nroBolitas :: Color -> MiniTablero -> Int
nroBolitas = undefined


-- Indica si hay bolitas de un color en la celda actual
hayBolitas :: Color -> MiniTablero -> Bool
hayBolitas = undefined
