module Tarea08 where
{- TEMA: INTRO A TIPOS DE DATOS ALGEBRAICOS
   CANTIDAD DE UNDEFINEDs: 3
   Román García -}

{- NOTAS:
   * Un tipo de datos algebraicos (data) introduce al lenguaje un nuevo conjunto
     de valores sobre los que se pueden trabajar.
   * Un sinonimo de tipos (type) nos permite referirnos a un tipo conocido de
     con otro nombre para mejorar la legibilitad de nuestro codigo. No introduce
     un nuevo tipo de datos.
   * Inclusive si en el ejercicio no se pide hacer las funciones de acceso,
     muchas veces conviene hacer igualmente, porque las funciones quedan
     mas legibles.
-}

-- Supongamos que queremos modelar las facturas de una empresa de la siguiente
-- forma:

data TipoFactura = A | B | C
data Factura = MkFac TipoFactura Numero Importe
type Numero = Int
type Importe = Double

-- Construyo algunos valores del tipo algebraico
fac1 = MkFac A 153  3000.0
fac2 = MkFac A 154  254.0
fac3 = MkFac B 2322 400.0
facturas = [fac1,fac2,fac3]


-- Dada una lista de facturas, determinar la suma.
--TEST: sumaFacturas facturas -> 3654.0
sumaFacturas :: [Factura] -> Double
sumaFacturas   []   = 0.0
sumaFacturas (f:fs) = importe f + sumaFacturas fs

--auxiliar: Calculo el importe de una factura. Esta es una funcion de acceso
importe :: Factura -> Importe
importe (MkFac t n i) = i


-- Determinar si dos facturas son del mismo tipo
--TEST: sonDelMismoTipo fac1 fac2 -> True
--TEST: sonDelMismoTipo fac1 fac3 -> False
sonDelMismoTipo :: Factura -> Factura -> Bool
sonDelMismoTipo = undefined


-- Determinar si una factura viene antes que otra en el talonario. Para ello,
-- deben tener el mismo tipo y el numero de la primera es menor que la segunda
--TEST: vieneAntesEnElTalonario fac1 fac2 -> True
--TEST: vieneAntesEnElTalonario fac2 fac1 -> False
vieneAntesEnElTalonario :: Factura -> Factura -> Bool
vieneAntesEnElTalonario = undefined


-- Dada una lista de facturas, determinar si estan ordenadas, o seo, que
-- que la factura en posicion n viene antes en el talonario que la (n+1)
-- TEST: estanOrdenadas [fac1,fac2] -> True
-- TEST: estanOrdenadas [fac1,fac2,fac3] -> False
estanOrdenadas :: [Factura] -> Bool
estanOrdenadas = undefined
