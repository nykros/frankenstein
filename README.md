# FRANKENSTEIN: Estructuras + Haskell + git

Este repo contiene ejercicios en Haskell para la materias Estructuras de Datos de la UNQ.

## Objetivos

Frankenstein consiste en 26 tareas, en orden creciente de dificultad, que cubren el contenido de la parte de Haskell de Estructuras de Datos.
Cada tarea tiene un conjunto de valores y funciones definidos. Algunas funciones y valores están resueltos (para servir de ejemplos) y otros solamentes tienen como cuerpo un "_undefined_", y deben ser completados.

Así, la tarea principal en Frankenstein consiste en “matar los _undefined_”. Un _undefined_ representa un espacio en blanco que se debe completar con código. Cada tarea tiene entre 3 y 7 _undefined_.

## Instalación

- Si no se tiene Stack instalado, instalarlo, siguiendo las instrucciones de [Cómo instalar Stack](haskell-env/stack.md).
- Ir al directorio donde se clonó a Frankestein y tipear `stack setup`. Esto descargará el compilador de Haskell y las librerías que necesita el proyecto automáticamente. Se debe tener una buena conexión a internet porque sino puede tardar mucho. Con una conexión de 5Mbit tarda menos de 5 minutos.
- Para compilar el proyecto, tipear `stack build`
- Para ejecutar el proyecto, tipear `run` o `stack exec frankenstein`

## Usando el intérprete

Para levantar el intérprete, tipear `stack ghci`. Esto cargará el interprete con todas las tareas, lo que permitirá evaluar expresiones como:

```
>esPar 44
True

>:t fac1
fac1 :: Factura

>sucesor 5
Exception: Prelude.undefined .... bla bla bla

>:q
Leaving GHCi
```

- En el primer input evaluamos expresión "esPar 44" que evalua al valor de verdad True.
- En el segundo input consultamos el tipo de la expresión "fac1" que es de tipo Factura.
- En el tercer input lanza una excepción porque todavía no se ha "matado" ese _undefined_.
- En el último input salimos del inteprete.

---

Autor: Román García (nykros@gmail.com)
