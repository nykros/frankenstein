# Cómo usar Haskell con Atom

Atom es un moderno editor de texto utilizado para programar en diferentes lenguajes a través de la enorme colección de extensiones que se le puede agregar. Para usarlo con Haskell vamos a tener que trabajar un poco, pero vale la pena.

### Instalación

- Bajar el editor ATOM (https://atom.io/)
- Correr: `stack install ghc-mod hlint stylish-haskell`
- En Atom, instalar los siguientes packages (File|Settings). Conviene buscar la palabra haskell:
    - language-haskell
    - ide-haskell
    - ide-haskell-repl
    - haskell-ghc-mod
 - En el package ide-haskell-repl, elegir "Stack" para la opcion "Default Repl"
 - En el package ghc-mod, poner en "Aditional Path Directories" "C:\Users\Roman\AppData\Roaming\local\bin"  
 - Cerrar y abrir Atom

 ------------------------------------------------------------------
 ### Opcional
 Agregar en File|Keymaps los siguientes mapeos de teclas:

 ```
 "atom-text-editor.ide-haskell-repl":
   'enter': 'ide-haskell-repl:exec-command'
   'up': 'ide-haskell-repl:history-back'
   'down': 'ide-haskell-repl:history-forward'
   'shift-enter': 'ide-haskell-repl:ghci-reload'
   'ctrl-shift-c': 'ide-haskell-repl:ghci-interrupt'
```
