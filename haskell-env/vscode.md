# Cómo usar Haskell con VSCode

VSCode es un moderno editor de texto utilizado para programar en diferentes lenguajes a través de la enorme colección de extensiones que se le puede agregar. Para usarlo con Haskell vamos a tener que trabajar un poco, pero vale la pena.

### Instalación

- Bajarse VSCode de https://code.visualstudio.com/
- Agregar %HOME%\.local\bin al path
- En el archivo `C:\sr\global-project\stack.yaml` verificar que diga `resolver: lts-14.27` u otra versión no demasiado reciente del LTS y agregar la líne `allow-newer: true`.
- En la línea de comando, fuera de cualquiera de nuestros proyectos, tipear `stack install intero QuickCheck hlint stack-run ghcid`. Esto hará que se creen algunas componentes que el VSCode necesitará para trabajar. Puede tardar varios minutos.
- Instalar las siguientes extensiones en VSCode:
    - Haskelly
    - haskell-linter
    - hoogle-vscode


Con esto, obtenemos a un VSCode con
 - Uso de colores en la sintaxis del lenguaje
 - Snippets y sugerencias
 
