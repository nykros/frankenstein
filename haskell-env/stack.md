# Stack

Stack crea un ambiente de trabajo aislado para cada uno de nuestro proyectos en Haskell. Es posible tener en la misma computadora proyectos con versiones diferentes del compilador y de las librerías sin que interfieran entre sí. La idea es similar al npm de JavaScript, al maven de Java o pip de Phyton.

## 1- Cómo instalar Stack

- Descargar Stack de https://docs.haskellstack.org/en/stable/install_and_upgrade/#windows (Seleccionar si se quiere para Windows de 64 bits o 32 bits).

![Elegir entre Windows 64 bit installer o Windows 32 bits installers](1.png)

- Instalar Stack en el directorio por defecto. No tocar las opciones. Siguiente(Next)... Siguiente(Next)... Cerrar(Close).

![Clicks](2.png)

## 2- Cómo crear un proyecto Stack

- Para crear un proyecto stack tipear `stack new miproyecto`. Esto creará un nuevo directorio y lo preparará para usar la última versión de haskell y librerías disponible. Muchas veces esto no es lo mejor, o se quiere usar las misma librerias que otro projecto, entonces se debe especificar el resolver.
- Para crear un proyecto con el mismo Haskell y librerías que Frankestein, tipear `stack new miproyecto --resolver lts-8.16`
- El projecto creado es una "cáscara". Para descargar el compilador y librerías tipear `stack setup`

## Dónde escribir nuestras funciones

Normamente ponemos nuestro código en la función main del archivo Main.hs ubicado en el subdirectorio app.

```Haskell
module Main where

import Lib

main :: IO ()
main = print (segundosEnDias 1)

segundosEnDias :: Int -> Int
segundosEnDias cantDias = cantDias * 24 * 60 * 60
```

## Cómo ejecutar nuestro proyecto

- Podemos compilar el proyecto con `stack build`
- Podemos ejecutar el proyecto con `stack run miproyecto`
- Podemos jugar con el intérprete y nuestra funciones con `stack ghci`

```
*Main Lib> segundosEnDias 3
259200
*Main Lib> :q
Leaving GHCi.
```
